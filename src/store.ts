import { writable, readable, type Writable } from 'svelte/store';

type Answer = "correct" | "incorrect" | "none";

export const ended = writable(false);

export const questions = readable([
    {
        real: "Sketchy Rendering for Information Visualization",
        realDoi: "10.1109/TVCG.2012.262",
        fake: "Spurious exploratory analysis of inputs",
    },
    {
        real: "TenniVis: Visualization for Tennis Match Analysis",
        realDoi: "",
        fake: "HyDraw: Better visual guidance for hydrolysis",
    },
]);
export const answers: Writable<Array<{ answered: boolean, answer: Answer }>> = writable([
    {
        answered: false,
        answer: "none"
    },
    {
        answered: false,
        answer: "none"

    },
]);